const express = require('express');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser')
const passport = require( 'passport');

require('./models/User'); //registra el schema con mongoose
require('./services/passport');

//conectar mongoose
const keys = require('./config/keys');
const mongoose = require('mongoose');
mongoose.connect(keys.mongoURL);

const app = express();

app.use(bodyParser.json()); 
app.use(cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
}));
app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/billingRoutes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT);
console.log('Emaily app listening on port ' +PORT);