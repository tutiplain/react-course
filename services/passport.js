const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const keys = require('../config/keys');
const mongoose = require('mongoose');


//forma para traer el modelo sin crear una segunda instancia de mongoose
const User = mongoose.model('users');

//funcion para serializar usuario y mandarlo en el cookie
passport.serializeUser( (user,done) =>{  //este user es el q se pasa en el callback del oauth flow
    done(null,user.id);
});

//para obtener el usuario nuevamente
passport.deserializeUser((id,done) =>{
    User.findById(id)
    .then (user => { 
        done(null,user); 
    })
})

passport.use(
    new GoogleStrategy({
        clientID: keys.googleClientID,
        clientSecret:keys.googleClientSecret,
        callbackURL: '/auth/google/callback',
        proxy:true //esto obliga a usar https
    }, 
    async (accessToken,refeshToken,profile,done) => {
        
       const existingUser =await User.findOne( { googleId: profile.id})
        
       if (existingUser)
       {
            //no new user required
            console.log('User already exists.\n');
            done(null,existingUser);  //primer argumento es un error object, null pq fue exitosa la operacion
       }
       else {
            const user = await new User( {googleId: profile.id}).save()
            done(null,user);
       }
    
    })
);
