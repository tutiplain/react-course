// para escoger entre los keys de production o dev
if (process.env.NODE_ENV =='production')
{
    module.exports = require('./prod');
}
else
{
    //usa los dev keys
    module.exports = require('./dev');
}