import axios from 'axios';
import { FETCH_USER } from './types';

/*
El Flow de Redux es: 
1. Components llaman a los Action Creators
2. El Action Creator (como este archivo) genera un Action (un objeto JS con una propiedad "type" y un payload). 
3. Normalmente el Action Creator simplemente hace un return del Action, pero gracias al reduxThunk middleware, se devuelve una función que tiene dentro el "dispatch" 
   function como argumento
4. la función dispatch le envía el Action a los Reducers
5. Los reducers alteran el state de la aplicación (también llamado "store"), y actualizan los Components
*/

//este action creator se usa al principio del app para buscar el user model del usuario logueado en el app
export const fetchUser = () =>
    async dispatch =>{
    const res = await axios.get('/api/current_user');
    dispatch({type: FETCH_USER,payload:res.data || ""});
};
    

//este action creator manejará el token que recibimos de Stripe al momento de pagar usando react-stripe-checkout
export const handleToken = (token) => async dispatch =>{
    console.log("This is the token:");
    console.log(token);
    const res = await axios.post ('/api/stripe',token);

    //como el modelo del user va a tener el numero de créditos, reusamos el mismo tipo de action
    dispatch({type: FETCH_USER,payload:res.data || ""});
}