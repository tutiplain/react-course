import 'materialize-css/dist/css/materialize.min.css'
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk'; //para obtener acceso al dispatch function dentro de los Action Reducers (recuerda que tienen q devolver un function)


import App from './components/App';
import reducers from './reducers'  //como esto es un folder, importa el index.js de ahí (que tiene el combineReducers)

//redux setup
const store = createStore(reducers , //primer arugmento: esta función recibe los "reducers" como argumento
{}, // initial state
applyMiddleware(reduxThunk) //tercer argumento para aplicar los middle wares
); 

ReactDOM.render(
    <Provider store ={store}><App /></Provider>, //elemento "root" del react-app. El Provider (de redux) permite a todos los componentes compartir el state
    document.querySelector('#root') //elemento del HTML donde el react-app aparecerá (ver index.html en client/public) 
);