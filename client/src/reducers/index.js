import {combineReducers} from 'redux';
import authReducer  from './authReducer';

//las propiedades del objeto que se pasa a combineReducers representan las propiedades del state del app
export default combineReducers({
    auth: authReducer
});