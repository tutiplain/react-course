import {FETCH_USER} from '../actions/types';

//se pone el default = null para ese momento cuando el request a /api/current_user aun no ha resuelto
export default function(state=null, action) 
{
    switch(action.type)
    {
        case FETCH_USER:
            return action.payload || false;  //esto provee para los tres casos, not logged in, logged in y el null para cuando aun no sabemos
        default:
            return state;
            
    }
}