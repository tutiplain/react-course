import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'; //Link es un react component para que React Router pueda manejar por los diferentes routes del app
import Payments from './Payments';

class Header extends Component {

    //metodo que cambia la porcion del header dependiendo de si el usuario está logueado
    renderContent()
    {   
        switch(this.props.auth)
        {
            case null:
                return;
            case false:
                return (
                    <li><a href="/auth/google">Login with Google</a></li>
                )
            default:
                return [
                    <li key="1"><Payments /></li>,
                    <li ley="2"><a href="/api/logout">Log out</a></li>];
                
        }
    }

    render() {
        console.log(this.props);
        // se usa un turnary expression para saber a qué route vamos a redirigir el usuario
        return(
            <nav>
                <div className="nav-wrapper">
                    <Link to={this.props.auth ? '/surveys': '/'} className="left brand-logo">Emaily</Link>
                    <ul className="right">
                        {this.renderContent()}
                    </ul>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state)
{
    return {auth: state.auth};  //state.auth se asigna en reducers/index, donde en la funcion de combineReducers se asigna el reducer a la propiedad auth del state
}

export default connect(mapStateToProps)(Header);