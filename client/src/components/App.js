import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Header from './Header';
import Landing from './Landing';
import {connect} from 'react-redux';
import * as actions from  '../actions'; //esto importa todos los action creators

//BrowserRouter solo puede tener un child element.


const Dashboard = () => <h2>Dashboard</h2>
const SurveyNew= () => <h2>SurveyNew</h2>


//el classname="container" es para que materialize css haga spacing en el borde"
class  App extends Component {
    
    /*Este life-cycle method se llama cuando el componente es dibujado a la pantalla por primera vez*/
    componentDidMount() {
        this.props.fetchUser(); //al usar el connect, ahora las funciones de los action creators son props del componente
    }
    
    render() {
        return(
            <div className="container">
                <BrowserRouter>
                    <div>
                        <Header />
                        <Route exact = {true} path="/" component={Landing} />
                        <Route exact path="/surveys" component={Dashboard} />
                        <Route path="/surveys/new" component={SurveyNew} />
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

//el connect() es quien ata los action creators con el App Component
export default connect(null,actions)(App); //el default causa que si haces require, el objeto devuelto sea = al que esta nombreado como default