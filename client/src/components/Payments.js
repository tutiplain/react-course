import React, {Component} from 'react';
import StripeCheckout from 'react-stripe-checkout';
import {connect} from 'react-redux';
import * as actions from '../actions';

class Payments extends Component{
    render(){
        console.log("Stripe key is:"+ process.env.REACT_APP_STRIPE_KEY);
        //amount = centavos
        //token call back que viene con el token enviado por Stripe
        return(
            <StripeCheckout 
            stripeKey={process.env.REACT_APP_STRIPE_KEY}
            name="Emaily"
            description="$5 for 5 survey credits"
            amount={500}
            token = { token => this.props.handleToken(token) /*llama al action creator */}
            >
                <button className="btn">
                    Add credits
                </button>
            </StripeCheckout>
        );

    }
}

export default connect(null,actions)(Payments);
